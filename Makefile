.PHONY = all ctags compile run help
CFLAGS=-Wall -Wextra -std=c11 -pedantic -ggdb


all: ctags compile run ##

ctags: main.c ## Generate ctags for Emacs
	ctags -e main.c

compile: main.c ##
	$(CC) $(CFLAGS) main.c -o main

run: main ##
	./main

help:
	@echo ""
	@echo "Usage: make [target]"
	@echo ""
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/$$//' | sed -e 's/##//'
	@echo ""
