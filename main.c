#include <stdio.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>

#define QUEUE_EMPTY INT_MIN

typedef struct node {
    int value;
    struct node* next;
} node;

typedef struct queue {
    char* name;
    node* head;
    node* tail;
} queue;

void init_queue(queue *q, char* name) {
    q->name = name;
    q->head = NULL;
    q->tail = NULL;
}

bool enqueue(queue* q, int value) {
    node *n = malloc(sizeof(node));
    if (n == NULL) {
        return false;
    }

    n->value = value;
    n->next = NULL;

    if (q->tail != NULL) {
        q->tail->next = n;
    }
    q->tail = n;

    if (q->head == NULL) {
        q->head = n;
    }
    return true;
}

int dequeue(queue* q) {
    if (q->head == NULL) {
        printf("Failed to dequeue from %s. The queue is empty!\n", q->name);
        return QUEUE_EMPTY;
    }

    node *tmp = q->head;
    int result = tmp->value;

    q->head = tmp->next;

    if (q->head == NULL) {
        q->tail = NULL;
    }

    printf("Dequeuing %s. value = %d\n", q->name, result);

    free(tmp);
    return result;
}

void print_queue(queue *q) {
    printf("%s: ", q->name);
    if (q->head == NULL) {
        printf("%s\n", "The queue is empty");
        return;
    }

    node *n = q->head;
    while (n != NULL) {
        printf("%d ", n->value);
        n = n->next;
    }

    printf("\n");
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    queue s1, s2, s3;

    init_queue(&s1, "s1");
    init_queue(&s2, "s2");
    init_queue(&s3, "s3");
    enqueue(&s1, 56);
    enqueue(&s1, 57);
    enqueue(&s2, 78);
    enqueue(&s2, 23);
    enqueue(&s3, 988);
    enqueue(&s3, 13);

    print_queue(&s1);
    print_queue(&s2);
    print_queue(&s3);

    dequeue(&s1);
    dequeue(&s1);
    dequeue(&s1);

    return 0;
}
